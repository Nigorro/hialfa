import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-upper',
  templateUrl: './upper.component.html',
  styleUrls: ['./upper.component.scss']
})
export class UpperComponent implements OnInit {
  number:number;
  quantity: number;

  constructor(private dataService: DataService ) {
    
  }

  numberChange(event) {
    this.dataService.number = +event.target.value;
  }
  quantityChange(event) {
    this.dataService.quantity = +event.target.value;
  }

  onSubmit(form: NgForm) {

    if(!form.valid) return

    this.dataService.number = form.value.number;
    this.dataService.quantity = form.value.quantity;
  }
  ngOnInit() {
    this.number = this.dataService.number;
    this.quantity = this.dataService.quantity;
  }
}

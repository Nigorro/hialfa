import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _number: number = 10;
  private _quantity: number = 5

  constructor() { }

  get number() {
    return this._number;
  }

  set number(number: number) {
    this._number = number;
  }

  get quantity() {
    return this._quantity;
  }

  set quantity(quantity: number) {
    this._quantity = quantity;
  }

  counter () {
    return new Array(this._quantity);
  }
}

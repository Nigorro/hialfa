import { Component, OnInit, SimpleChanges } from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-bottom-item',
  templateUrl: './bottom-item.component.html',
  styleUrls: ['./bottom-item.component.scss']
})
export class BottomItemComponent implements OnInit {

  constructor(private dataService: DataService ) {
  }
  ngOnInit() {
  }

}

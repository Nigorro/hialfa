import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { AlfaComponent } from './alfa/alfa.component';
import { UpperComponent } from './upper/upper.component';
import { BottomComponent } from './bottom/bottom.component';
import { BottomItemComponent } from './bottom-item/bottom-item.component';

@NgModule({
  declarations: [
    AppComponent,
    AlfaComponent,
    UpperComponent,
    BottomComponent,
    BottomItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
